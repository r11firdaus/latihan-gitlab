class Article < ApplicationRecord
	has_many :comments, dependent: :destroy #kalau post di delete, comment juga di delete
	validates :title, presence: true, length: { minimum: 5 }
end
